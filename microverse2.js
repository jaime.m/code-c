function gradingStudents(grades) {
    // Write your code here
     const newGrades =grades.map(x=>{
         const diff=Math.ceil(x/10)*10-x;
         const rDiff=diff>5?diff-5:diff;
         if(rDiff<3 && x>=38){
             return x+rDiff;
         }else
            return x;
    }) 

    return newGrades;

}
const p=
gradingStudents([73,
    67,
    38,
    33]);
    console.log(p);